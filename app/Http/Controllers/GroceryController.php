<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grocery;
use App\GroceryList;
use App\Http\Requests\StoreGrocery;

class GroceryController extends Controller
{
    // lists all products off the grocery list
    public function index()
    {
        $groceries = Grocery::all();
        $groceryList = new GroceryList($groceries);
        return view('groceries.index', compact([
            'groceryList'
        ]));
    }

    // shows a form for adding a new product
    public function create()
    {
        return view('groceries.create');
    }

    // validates and saves a submitted product to the database
    public function store(StoreGrocery $request)
    {
        Grocery::create($request->validated()); // fill with validated inputs
        return redirect()->route('groceries.index');
    }

    // shows and fills a form with an existing product
    public function edit(Grocery $grocery)
    {
        return view('groceries.edit', compact([
            'grocery'
        ]));
    }

    // saves updates to an existing product
    public function update(StoreGrocery $request, Grocery $grocery)
    {
        $grocery->update($request->validated());

        return redirect()->route('groceries.index');
    }

    // deletes a product
    public function destroy(Grocery $grocery)
    {
        Grocery::destroy($grocery->id);

        return redirect()->route('groceries.index');
    }
}
