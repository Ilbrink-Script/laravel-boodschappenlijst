<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grocery extends Model
{
    protected $fillable = ['title', 'quantity', 'price'];

    public function getSubTotal()
    {
        return number_format($this->quantity * $this->price, 2);
    }
}
