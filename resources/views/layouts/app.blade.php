<!DOCTYPE html>
<html lang="en">

@section('head')

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Opdracht 19: Laravel boodschappenlijst</title>
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/script.js"></script>
</head>
@show

@section('body')

<body>
    @section('nav')
    <nav>
        <ul>
            <li><a href="{{ route('groceries.index') }}">Overzicht</a></li>
            <li><a href="{{ route('groceries.create') }}">Invoer</a></li>
    </nav>
    @show

    @yield('content')
</body>
@show

</html>