@extends('layouts.app')

@section('content')
    <table>
        <thead>
            <tr>
                <th>Product</th>
                <th>Prijs</th>
                <th>Aantal</th>
                <th>Subtotaal</th>
                <th>Wijzig</th>
                <th>Verwijder</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($groceryList->getItems() as $grocery)
                <tr class="product-{{ $grocery->id }}">
                    <td>{{ $grocery->title }}</td>
                    <td class="productPrice">{{ $grocery->price }}</td>
                    <td>
                        <button>-</button>
                        <input class="productQuantity" type="number" min="0" value="{{ $grocery->quantity }}">
                        <button>+</button>
                    </td>
                    <td class="productTotalCost">{{ $grocery->getSubTotal() }}</td>
                    <td><a href="{{ route('groceries.edit', $grocery->id) }}">&#9999;</a></td>
                    <td>
                        <form action="{{ route('groceries.destroy', $grocery->id) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit">&#9760;</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Totaal</td>
                <td id="totalCost">{{ $groceryList->getTotal() }}</td>
                <th colspan="2"></th>
            </tr>
        </tfoot>
    </table>
@endsection('content')
