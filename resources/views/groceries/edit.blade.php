@extends ('layouts.app')

@section('content')

<form id="create" action="{{ route('groceries.update', $grocery->id) }}" method="POST">
        @method('PUT')
        @csrf

        @include('groceries.form-fields')

        <button>Wijzigen</button>
    </form>

@endsection
