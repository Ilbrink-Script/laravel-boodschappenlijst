<label for="title">Naam</label>
<input type="text" @error('title') class="error" @enderror id="title" name="title" placeholder="Product naam"
    value="{{ old('title', isset($grocery) ? $grocery->title : '') }}" required>
@error('title')
<span class="error">{{ $errors->first('title') }}</span>
@enderror

<label for="quantity">Aantal</label>
<input type="number" @error('quantity') class="error" @enderror id="quantity" name="quantity" placeholder="1" min="1"
    value="{{ old('quantity', isset($grocery) ? $grocery->quantity : '') }}" required>
@error('quantity')
<span class="error">{{ $errors->first('quantity') }}</span>
@enderror


<label for="price">Prijs</label>
<input type="number" @error('price') class=error @enderror id="price" name="price" placeholder="1.00" min="0.00"
    step="0.01" value="{{ old('price', isset($grocery) ? $grocery->price : '') }}" required>
@error('price')
<span class="error">{{ $errors->first('price') }}</span>
@enderror