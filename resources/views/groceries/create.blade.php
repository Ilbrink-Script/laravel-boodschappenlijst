@extends('layouts.app')

@section('content')

    <form id="create" action="{{ route('groceries.store') }}" method="POST">
        @csrf

        @include('groceries.form-fields')

        <button>Versturen</button>
    </form>
@endsection
