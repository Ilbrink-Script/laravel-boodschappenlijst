<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('groceries', 'GroceryController@index')->name('groceries.index');
Route::get('groceries/create', 'GroceryController@create')->name('groceries.create');
Route::post('groceries', 'GroceryController@store')->name('groceries.store');
Route::get('groceries/{grocery}/edit', 'GroceryController@edit')->name('groceries.edit');
Route::put('groceries/{grocery}', 'GroceryController@update')->name('groceries.update');
Route::patch('groceries/{grocery}', 'GroceryController@update')->name('groceries.update'); //do I need to use both?
Route::delete('groceries/{grocery}', 'GroceryController@destroy')->name('groceries.destroy');

Route::permanentRedirect('/', 'groceries');
