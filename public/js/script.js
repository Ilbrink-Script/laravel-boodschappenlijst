let inputQtyArr = [];

// adds event handler to product inputs
const initProductHandlers = () => {
    // list of quantity inputs
    inputQtyArr = document.getElementsByClassName('productQuantity');
    if (inputQtyArr.length === 0) {
        return;
    } 

    let inputQty;
    for (let i = 0; i < inputQtyArr.length; i++) {
        inputQty = inputQtyArr[i];
        addQuantityListener(inputQty, i);
    }

    // iterate buttons
    const buttonArr = document.getElementsByTagName('button');
    let btn, productIdx;
    for (let i = 0; i < buttonArr.length; i++) {
        btn = buttonArr[i];
        productIdx = Math.floor(i / 2);
        addButtonListener(btn, productIdx);
    }
};

// add event listener to input quantity element
const addQuantityListener = (element, productIdx) => {
    element.addEventListener('change', (event) => {
        const amount = event.target.value;
        console.log('aantal producten is gewijzigd');
        updateProductSubTotal(productIdx);
    });
};

// add event listener to product buttons
const addButtonListener = (element, productIdx) => {
    element.addEventListener('click', (event) => {
        const operator = element.textContent;
        let input = inputQtyArr[productIdx];
        // ehm.. do it this way maybe?
        input = document.getElementsByClassName('productQuantity')[productIdx];

        // mutate input depending on the operator
        switch (operator) {
            case '+':
                input.stepUp();
                break;
            case '-':
                input.stepDown();
                break;
            default:
                break;
        }

        // why isn't the change event triggered?
        updateProductSubTotal(productIdx);
    });
};

// gets the price for product at index
const getProductPrice = productIdx => {
    const cell = document.getElementsByClassName('productPrice')[productIdx];
    return parseFloat(cell.textContent);
};

// gets the price for product at index
const getProductQuantity = productIdx => document.getElementsByClassName('productQuantity')[productIdx].value;

// sets the subtotal for product at index and returns the previous subtotal
const setProductTotalCost = (productIdx, totalCost) => {
    const cell = document.getElementsByClassName('productTotalCost')[productIdx];
    const prevTotal = parseFloat(cell.textContent);
    cell.textContent = fmtPrice(totalCost);
    return prevTotal;
};

// gets the total cost for all products
const getTotalCost = () => {
    const totalCost = parseFloat(document.getElementById('totalCost').textContent);
    return totalCost;
};

// sets the total cost for all products or mutates it
const setTotalCost = (totalCost, isMutation) => {
    const cell = document.getElementById('totalCost');
    if (!cell) {
        return;
    }

    let newTotal = totalCost;
    if (isMutation) {
        const prevTotal = getTotalCost();
        newTotal += prevTotal;
    }
    cell.textContent = fmtPrice(newTotal);
};

// updates the subtotal for a product and returns it as well
const updateProductSubTotal = productIdx => {
    const price = getProductPrice(productIdx);
    const qty = getProductQuantity(productIdx);
    const productTotal = price * qty;

    // mutate product total (so there's no need to recalculate it)
    const prevProductTotal = setProductTotalCost(productIdx, productTotal);
    const totalMutation = productTotal - prevProductTotal;
    setTotalCost(totalMutation, true);
};

// (re)calculates the total price for the whole grocery list
const calculateTotalPrice = () => {
    const subTotals = document.getElementsByClassName('productTotalCost');
    let total = 0;
    for (let i = 0; i < subTotals.length; i++) {
        total += parseFloat(subTotals[i].textContent);
    }
    setTotalCost(total);
};

// formats a price with 2 decimals
const fmtPrice = price => {
    return price.toFixed(2);
};


// initialize scripts
window.onload = (event) => {
    initProductHandlers();
    calculateTotalPrice();
}